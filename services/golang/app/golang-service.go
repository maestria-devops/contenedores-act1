package main

import (
	"fmt"
	"io"
	"net/http"	
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Docker Service with Golang!\n")
}

func main() {
	http.HandleFunc("/", getRoot)
    fmt.Printf("Server running on port 3333\n")
	http.ListenAndServe(":3333", nil)    
}