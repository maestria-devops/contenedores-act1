#! /bin/sh
export MAVEN_IMAGE=maven:3-eclipse-temurin-11
docker pull $MAVEN_IMAGE

docker run -it --rm -v "$(pwd)/app":/usr/src/java -w /usr/src/java $MAVEN_IMAGE mvn clean package