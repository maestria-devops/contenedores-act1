#! /bin/sh
export set DOCKER_FILE=Dockerfile
export set DOCKER_REGISTRY=anayaj0511
export set GROUPID=masterdevops-java
export set COMPONENT=httpservice
export set VERSION=1.0.0.RELEASE
export set DOCKER_TAG=$DOCKER_REGISTRY/$GROUPID-$COMPONENT:$VERSION

echo -----------------------------------------------------------------
echo Builing Enviroment....
echo DOCKER_FILE = $DOCKER_FILE
echo GROUPID = $GROUPID
echo COMPONENT = $COMPONENT
echo VERSION = $VERSION
echo DOCKER_TAG = $DOCKER_TAG
echo -----------------------------------------------------------------

docker build -f $DOCKER_FILE -t $DOCKER_TAG . --build-arg JAR_FILE=./app/target/http-service-1.0.0.jar

echo pushing image...
docker push $DOCKER_TAG